package com.codingapi.txlcn.common.util;

import java.util.Locale;

public class classUtils {

    public static void printCaller() {
        try {
            StackTraceElement[] trace = new Throwable().fillInStackTrace().getStackTrace();
            String caller = "";
            String callingClass = "";
            String callFile = "";
            int lineNumber = 0;
            System.out.println("==========BEGIN OF CALLER INFO============");
            for (int i = 2; i < trace.length; i++) {
                callingClass = trace[i].getClassName();
                callingClass = callingClass.substring(callingClass
                        .lastIndexOf('.') + 1);
                caller = trace[i].getMethodName();
                callFile = trace[i].getFileName();
                lineNumber = trace[i].getLineNumber();
                String method = String.format(Locale.US, "[%03d] %s.%s(%s:%d)"
                        , Thread.currentThread().getId(), callingClass, caller, callFile, lineNumber);
                System.out.println(method);
            }
            System.out.println("==========END OF CALLER INFO============");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
